package dev.atomicdevelopment.teamsort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class TeamSort {
    private final List<String> members;
    private final int teamCount;

    public TeamSort(int teamCount, List<String> members) {
        this.members = members;
        this.teamCount = teamCount;
    }

    public List<HashSet<String>> sort() {
        List<HashSet<String>> teams = new ArrayList<>();
        int membersPerTeam = (int) Math.floor((float) members.size() / (float) teamCount);
        Collections.shuffle(members);
        int lastSublist = 0;
        int nextSublist = membersPerTeam;
        for (int i = 0; i < teamCount; i++) {
            HashSet<String> teamMembers = new HashSet<>(members.subList(lastSublist, nextSublist));
            teams.add(teamMembers);
            lastSublist += membersPerTeam;
            nextSublist += membersPerTeam;
        }
        List<String> remainingMembers = members.subList(lastSublist, members.size());
        int teamIndex = 0;
        for (String member : remainingMembers) {
            if (teamIndex == teamCount - 1) {
                teamIndex = 0;
            }
            HashSet<String> team = teams.get(teamIndex);
            team.add(member);
            teams.set(teamIndex, team);
            teamIndex++;
        }
        return teams;
    }
}
