# Java Team Sort

## About:
Java Team Sort allows you to sort an array of members into a dynamic number of teams!

# How fast?
After JIT compilation, this algorithm can sort the example implementation in 33 microseconds, or 0.033ms

## Use:
there is an example in the Example directory, you will need to import the class into your program, jar archive can be found in releases!

## Example implementation:

```java
package dev.atomicdevelopment.ExampleImplementation;

import TeamSort;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Example {

    public static void main(String[] args) {
        String[] arrNames = {"Michael", "Omar", "Fred", "Isaac", "Berry"}; // example array of names
        List<String> names = Arrays.asList(arrNames);
        TeamSort teamSort = new TeamSort(2, names);
        List<HashSet<String>> sortedTeams = teamSort.sort();
        System.out.println(sortedTeams);
    }
}
```